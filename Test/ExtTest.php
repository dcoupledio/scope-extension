<?php

require('../vendor/autoload.php');

use phpunit\framework\TestCase;
use Decoupled\Core\Extension\Scope\ScopeExtension;
use Decoupled\Core\Application\Application;
use Decoupled\Core\Application\ApplicationContainer;

class ExtTest extends TestCase{

    public function testCanUseScopeExtentsion()
    {
        $app = new Application( new ApplicationContainer() );

        $app->uses( new ScopeExtension() );

        $scope = $app['$scope.factory']->make([
            'test'  => 1,
            'test2' => 2
        ]);

        $app->uses( $scope );

        $newScope = $app['$scope.new'];

        $scope = $app['$scope'];

        $this->assertEquals( $scope['test'], $newScope['test'] );
    }

}