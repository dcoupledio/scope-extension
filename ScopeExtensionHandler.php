<?php namespace Decoupled\Core\Extension\Scope;

use Decoupled\Core\Scope\Scope;
use Decoupled\Core\Application\ApplicationContainer;

class ScopeExtensionHandler{

    public function handle( Scope $scope, ApplicationContainer $app )
    {
        $app->extend( '$scope', function( $rootScope, $app ) use( $scope ){

            return $rootScope->merge( $scope );
        });

        return $app;
    }
}