<?php namespace Decoupled\Core\Extension\Scope;

use Decoupled\Core\Application\Application;
use Decoupled\Core\Application\ApplicationExtension;
use Decoupled\Core\Scope\ScopeFactory;
use Decoupled\Core\Scope\Scope;


class ScopeExtension extends ApplicationExtension{

    public function getName()
    {
        return 'scope.extension';
    }

    public function extend()
    {
        $app = $this->getApp();

        $app['$scope.factory'] = function(){

            return new ScopeFactory();
        };

        $app['$scope'] = function($container){

            return $container['$scope.factory']->make();
        };        

        $app['$scope.new'] = $app->factory(function($container){

            $scope = $container['$scope.factory']->make();

            $scope->setParent( $container['$scope'] );

            return $scope;
        });        

        $app->addExtensionTypeHandler([
            'Decoupled\Core\Scope\Scope'
            ], [ new ScopeExtensionHandler(), 'handle' ] );
            
    }
}